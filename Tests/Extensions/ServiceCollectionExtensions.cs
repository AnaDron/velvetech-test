using System;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Tests.Extensions {
	static class ServiceCollectionExtensions {
		public static IServiceCollection AddMockSingleton<T>(this IServiceCollection self, MockBehavior behavior, Action<Mock<T>> action = null) where T : class {
			var mock = new Mock<T>(behavior);
			action?.Invoke(mock);
			self.AddSingleton(mock.Object);
			return self;
		}

		public static IServiceCollection AddMockSingleton<T>(this IServiceCollection self, Action<Mock<T>> action = null) where T : class {
			return AddMockSingleton(self, MockBehavior.Default, action);
		}

		public static IServiceCollection AddStrictMockSingleton<T>(this IServiceCollection self, Action<Mock<T>> action = null) where T : class {
			return AddMockSingleton(self, MockBehavior.Strict, action);
		}

		public static IServiceCollection AddLooseMockSingleton<T>(this IServiceCollection self, Action<Mock<T>> action = null) where T : class {
			return AddMockSingleton(self, MockBehavior.Loose, action);
		}

		public static IServiceCollection AddMockScoped<T>(this IServiceCollection self, MockBehavior behavior, Action<Mock<T>> action = null) where T : class {
			return self.AddScoped(_ => {
				var mock = new Mock<T>(behavior);
				action?.Invoke(mock);
				return mock.Object;
			});
		}

		public static IServiceCollection AddMockScoped<T>(this IServiceCollection self, Action<Mock<T>> action = null) where T : class {
			return AddMockScoped(self, MockBehavior.Default, action);
		}

		public static IServiceCollection AddStrictMockScoped<T>(this IServiceCollection self, Action<Mock<T>> action = null) where T : class {
			return AddMockScoped(self, MockBehavior.Strict, action);
		}

		public static IServiceCollection AddLooseMockScoped<T>(this IServiceCollection self, Action<Mock<T>> action = null) where T : class {
			return AddMockScoped(self, MockBehavior.Loose, action);
		}
	}
}