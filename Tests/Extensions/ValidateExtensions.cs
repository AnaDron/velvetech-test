using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;

namespace Tests.Extensions {
	static class ValidateExtensions {
		static void Validate(this bool self, bool expected = false) {
			self.Should().Be(expected);
		}

		public static async Task Validate(this Task<bool> self, bool expected = false) {
			Validate(await self, expected);
		}

		public static async Task Validate(this Task<IDictionary<string, IList<string>>> self, bool expected = false) {
			Validate((await self).Count == 0, expected);
		}
	}
}