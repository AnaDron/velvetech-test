﻿using System;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Tests {
	[TestFixture]
	public abstract class TestBase {
		ServiceProvider _serviceProvider;

		protected IServiceProvider ServiceProvider => _serviceProvider;

		protected abstract void Setup(IServiceCollection sc);
		protected virtual void PostSetup() { }

		[SetUp]
		public void Setup() {
			var sc = new ServiceCollection();

			Setup(sc);

			_serviceProvider = sc.BuildServiceProvider();

			PostSetup();
		}

		protected virtual void BeforeTearDown() { }
		protected virtual void AfterTearDown() { }

		[TearDown]
		public void TearDown() {
			BeforeTearDown();

			_serviceProvider.Dispose();
			_serviceProvider = null;

			AfterTearDown();
		}
	}

	public abstract class TestBase<T> : TestBase where T : class {
		protected T Sut { get; private set; }

		protected override void PostSetup() {
			Sut = ServiceProvider.GetRequiredService<T>();
		}

		protected override void AfterTearDown() {
			Sut = null;
		}
	}
}