using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using Test.BLL;
using Test.Domain.Data;
using Test.Domain.Entities;
using Test.Domain.Models;
using Test.Domain.Services;
using Tests.Extensions;

namespace Tests.Services {
	public class StudentServiceTest : TestBase<IStudentService> {
		Mock<IStudentRepository> _mockStudentRepository;

		protected override void Setup(IServiceCollection sc) {
			sc.AddLogging();

			sc.AddBLL();

			sc.AddMockScoped<IStudentRepository>(x => _mockStudentRepository = x);
		}

		[Test]
		public async Task AddTest() {
			var model = new StudentAddModel();
			var exceptedResult = new Student();

			_mockStudentRepository.Setup(x => x.Add(It.IsAny<Student>()))
				.Returns((Student _) => Task.FromResult(exceptedResult));

			var result = await Sut.Add(model);

			result.Should().Be(exceptedResult);

			_mockStudentRepository.Verify(x => x.Add(It.IsAny<Student>()), Times.Once);
		}

		[Test]
		public async Task DeleteTest() {
			const int id = 0;

			await Sut.Delete(id);

			_mockStudentRepository.Verify(x => x.Delete(id), Times.Once);
		}
	}
}