using Test.Domain.Enums;

namespace Test.Domain.Entities {
	public class Student {
		public int Id { get; set; }

		public Gender Gender { get; set; }

		public string LastName { get; set; }
		public string FirstName { get; set; }
		public string Patronymic { get; set; }

		public string Uid { get; set; }
	}
}