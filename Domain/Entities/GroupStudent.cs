namespace Test.Domain.Entities {
	public class GroupStudent {
		public int GroupId { get; set; }
		public int StudentId { get; set; }
	}
}