using System.Threading.Tasks;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.Domain.Services {
	public interface IGroupService : ICrudService<Group, GroupAddModel, GroupUpdateModel> {
		Task<ListModel<GroupViewModel>> GetList(GroupGetListModel model);

		Task AddStudent(GroupStudent model);
		Task DeleteStudent(GroupStudent model);
	}
}