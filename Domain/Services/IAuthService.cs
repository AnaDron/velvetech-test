using System.Threading.Tasks;

namespace Test.Domain.Services {
	public interface IAuthService {
		Task<string> GenerateToken();
	}
}