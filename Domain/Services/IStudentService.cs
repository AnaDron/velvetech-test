using System.Threading.Tasks;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.Domain.Services {
	public interface IStudentService : ICrudService<Student, StudentAddModel, StudentUpdateModel> {
		Task<ListModel<StudentViewModel>> GetList(StudentGetListModel model);
	}
}