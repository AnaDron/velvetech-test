using System.Threading.Tasks;

namespace Test.Domain.Services {
	public interface ICrudService<T, in TCreate, in TUpdate> {
		Task<T> Add(TCreate model);
		Task<T> Get(int id);
		Task<T> Update(TUpdate model);
		Task Delete(int id);
	}
}