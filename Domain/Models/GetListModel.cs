namespace Test.Domain.Models {
	public class GetListModel<T> where T : new() {
		public class PaginationModel {
			public int Page { get; set; } = 0;
			public int PerPage { get; set; } = 10;
		}

		public T Filter { get; set; } = new T();

		public PaginationModel Pagination { get; set; } = new PaginationModel();
	}
}