using System.Collections.Generic;

namespace Test.Domain.Models {
	public class ListModel<T> {
		public IEnumerable<T> Data { get; set; }
	}
}