namespace Test.Domain.Models {
	public class GroupUpdateModel : GroupAddModel {
		public int Id { get; set; }
	}
}