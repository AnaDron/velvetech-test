namespace Test.Domain.Models {
	public class GroupGetListModel : GetListModel<GroupGetListModel.GroupGetListFilterModel> {
		public class GroupGetListFilterModel {
			public string Name { get; set; }
		}
	}
}