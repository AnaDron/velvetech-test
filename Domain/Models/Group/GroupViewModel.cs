namespace Test.Domain.Models {
	public class GroupViewModel {
		public int Id { get; set; }
		public string Name { get; set; }
		public int StudentsCount { get; set; }
	}
}