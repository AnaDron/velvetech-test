using Test.Domain.Enums;

namespace Test.Domain.Models {
	public class StudentGetListModel : GetListModel<StudentGetListModel.StudentGetListFilterModel> {
		public class StudentGetListFilterModel {
			public Gender? Gender { get; set; }
			public string Name { get; set; }
			public string Uid { get; set; }
			public string GroupName { get; set; }
		}
	}
}