namespace Test.Domain.Models {
	public class StudentViewModel {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Uid { get; set; }
		public string Groups { get; set; }
	}
}