using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.Domain.Data {
	public interface IGroupRepository : IRepository<Group> {
		Task<IEnumerable<GroupViewModel>> GetList(GroupGetListModel model);
	}
}