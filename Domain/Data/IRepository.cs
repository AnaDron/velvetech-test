using System.Threading.Tasks;

namespace Test.Domain.Data {
	public interface IRepository<T> {
		Task<T> Add(T entity);
		Task<T> Get(int id);
		Task<T> Update(T entity);
		Task Delete(int id);
	}
}