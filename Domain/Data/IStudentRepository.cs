using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.Domain.Data {
	public interface IStudentRepository : IRepository<Student> {
		Task<IEnumerable<StudentViewModel>> GetList(StudentGetListModel model);
	}
}