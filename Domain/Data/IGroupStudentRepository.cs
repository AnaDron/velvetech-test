using System.Threading.Tasks;
using Test.Domain.Entities;

namespace Test.Domain.Data {
	public interface IGroupStudentRepository {
		Task<GroupStudent> Add(GroupStudent entity);
		Task Delete(GroupStudent entity);
	}
}