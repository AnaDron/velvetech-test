﻿using AutoMapper;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.BLL {
	class AutoMapperProfile : Profile {
		public AutoMapperProfile() {
			#region Student

			CreateMap<StudentAddModel, Student>();
			CreateMap<StudentUpdateModel, Student>();

			#endregion
			
			#region Group

			CreateMap<GroupAddModel, Group>();
			CreateMap<GroupUpdateModel, Group>();

			#endregion
		}
	}
}