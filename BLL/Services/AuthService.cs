using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Test.Domain.Configuration;
using Test.Domain.Services;

namespace Test.BLL.Services {
	sealed class AuthService : IAuthService {
		readonly JwtBearerTokenSettings _settings;
		readonly ILogger _logger;

		public AuthService(
			IOptions<JwtBearerTokenSettings> settingsProvider,
			ILogger<AuthService> logger
		) {
			_settings = settingsProvider.Value;
			_logger = logger;
		}

		public Task<string> GenerateToken() {
			var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.SecretKey));

			var token = new JwtSecurityToken(
				_settings.Issuer,
				_settings.Audience,
				new[] {
					new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
				},
				expires: DateTime.Now.AddSeconds(_settings.ExpiryTimeInSeconds),
				signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
			);

			var result = new JwtSecurityTokenHandler().WriteToken(token);

			return Task.FromResult(result);
		}
	}
}