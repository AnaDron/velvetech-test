using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Test.Domain.Data;
using Test.Domain.Entities;
using Test.Domain.Models;
using Test.Domain.Services;

namespace Test.BLL.Services {
	sealed class StudentService : IStudentService {
		readonly IStudentRepository _repository;
		readonly IMapper _mapper;
		readonly ILogger _logger;

		public StudentService(
			IStudentRepository repository,
			IMapper mapper,
			ILogger<StudentService> logger
		) {
			_repository = repository;
			_mapper = mapper;
			_logger = logger;
		}

		public async Task<Student> Add(StudentAddModel model) {
			var entity = _mapper.Map<Student>(model);
			var result = await _repository.Add(entity);
			return result;
		}

		public async Task<Student> Get(int id) {
			var result = await _repository.Get(id);
			return result;
		}

		public async Task<ListModel<StudentViewModel>> GetList(StudentGetListModel model) {
			var data = await _repository.GetList(model);
			var result = new ListModel<StudentViewModel> { Data = data };
			return result;
		}

		public async Task<Student> Update(StudentUpdateModel model) {
			var entity = _mapper.Map<Student>(model);
			var result = await _repository.Update(entity);
			return result;
		}

		public async Task Delete(int id) {
			await _repository.Delete(id);
		}
	}
}