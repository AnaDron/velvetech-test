using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Test.Domain.Data;
using Test.Domain.Entities;
using Test.Domain.Models;
using Test.Domain.Services;

namespace Test.BLL.Services {
	sealed class GroupService : IGroupService {
		readonly IGroupRepository _repository;
		readonly IGroupStudentRepository _relationRepository;
		readonly IMapper _mapper;
		readonly ILogger _logger;

		public GroupService(
			IGroupRepository repository,
			IGroupStudentRepository relationRepository,
			IMapper mapper,
			ILogger<GroupService> logger
		) {
			_repository = repository;
			_relationRepository = relationRepository;
			_mapper = mapper;
			_logger = logger;
		}

		public async Task<Group> Add(GroupAddModel model) {
			var entity = _mapper.Map<Group>(model);
			var result = await _repository.Add(entity);
			return result;
		}

		public async Task<Group> Get(int id) {
			var result = await _repository.Get(id);
			return result;
		}

		public async Task<ListModel<GroupViewModel>> GetList(GroupGetListModel model) {
			var data = await _repository.GetList(model);
			var result = new ListModel<GroupViewModel> { Data = data };
			return result;
		}

		public async Task<Group> Update(GroupUpdateModel model) {
			var entity = _mapper.Map<Group>(model);
			var result = await _repository.Update(entity);
			return result;
		}

		public async Task Delete(int id) {
			await _repository.Delete(id);
		}

		public async Task AddStudent(GroupStudent model) {
			await _relationRepository.Add(model);
		}

		public async Task DeleteStudent(GroupStudent model) {
			await _relationRepository.Delete(model);
		}
	}
}