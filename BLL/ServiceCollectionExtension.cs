using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Test.BLL.Services;
using Test.Domain.Services;

namespace Test.BLL {
	public static class ServiceCollectionExtension {
		public static IServiceCollection AddBLL(this IServiceCollection self) {
			self.AddScoped<IAuthService, AuthService>();
			self.AddScoped<IStudentService, StudentService>();
			self.AddScoped<IGroupService, GroupService>();

			self.AddAutoMapper(typeof(AutoMapperProfile));

			return self;
		}
	}
}