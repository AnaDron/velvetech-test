using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Test.DAL;

namespace Test.Api {
	public static class Program {
		public static async Task Main(string[] args) {
			var host = CreateHostBuilder(args).Build();

			await host.Services.InitializeDAL();

			await host.RunAsync();
		}

		static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder => {
					webBuilder.UseStartup<Startup>();
				});
	}
}