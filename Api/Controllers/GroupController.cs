using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test.Api.Models;
using Test.Domain.Entities;
using Test.Domain.Models;
using Test.Domain.Services;

namespace Test.Api.Controllers {
	public class GroupController : AuthorizeController {
		readonly IGroupService _service;
		readonly IMapper _mapper;
		readonly ILogger _logger;

		public GroupController(
			IGroupService service,
			IMapper mapper,
			ILogger<GroupController> logger
		) {
			_service = service;
			_mapper = mapper;
			_logger = logger;
		}

		[HttpPost]
		public async Task<IActionResult> Add(GroupAddRequest request) {
			if (!ModelState.IsValid) return BadRequest();

			var serviceModel = _mapper.Map<GroupAddModel>(request);
			var result = await _service.Add(serviceModel);

			return Ok(_mapper.Map<GroupResponse>(result));
		}

		[HttpGet("{id}")]
		public async Task<GroupResponse> Get(int id) {
			var result = await _service.Get(id);
			return _mapper.Map<GroupResponse>(result);
		}

		[HttpPost("list")]
		public async Task<IActionResult> GetList(GroupGetListModel model) {
			if (!ModelState.IsValid) return BadRequest();

			var result = await _service.GetList(model);

			return Ok(result);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> Update(int id, GroupUpdateRequest request) {
			if (!ModelState.IsValid) return BadRequest();

			var serviceModel = _mapper.Map<GroupUpdateModel>(request);
			serviceModel.Id = id;
			var result = await _service.Update(serviceModel);

			return Ok(_mapper.Map<GroupResponse>(result));
		}

		[HttpDelete("{id}")]
		public Task Update(int id) {
			return _service.Delete(id);
		}


		[HttpPost("{groupId}/{studentId}")]
		public async Task AddStudent(int groupId, int studentId) {
			await _service.AddStudent(new GroupStudent { GroupId = groupId, StudentId = studentId });
		}

		[HttpDelete("{groupId}/{studentId}")]
		public async Task DeleteStudent(int groupId, int studentId) {
			await _service.DeleteStudent(new GroupStudent { GroupId = groupId, StudentId = studentId });
		}
	}
}