﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test.Domain.Services;

namespace Test.Api.Controllers {
	[AllowAnonymous]
	public sealed class AccountController : ApiController {
		readonly IAuthService _service;
		readonly ILogger _logger;

		public AccountController(
			IAuthService service,
			ILogger<AccountController> logger
		) {
			_service = service;
			_logger = logger;
		}

		[HttpPost("login")]
		public async Task<object> Login() {
			var token = await _service.GenerateToken();
			return new { token };
		}
	}
}