using Microsoft.AspNetCore.Mvc;

namespace Test.Api.Controllers {
	[ApiController]
	[Route("api/[controller]")]
	public abstract class ApiController : ControllerBase {
	}
}