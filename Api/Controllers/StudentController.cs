using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test.Api.Models;
using Test.Domain.Models;
using Test.Domain.Services;

namespace Test.Api.Controllers {
	public class StudentController : AuthorizeController {
		readonly IStudentService _service;
		readonly IMapper _mapper;
		readonly ILogger _logger;

		public StudentController(
			IStudentService service,
			IMapper mapper,
			ILogger<StudentController> logger
		) {
			_service = service;
			_mapper = mapper;
			_logger = logger;
		}

		[HttpPost]
		public async Task<IActionResult> Add(StudentAddRequest request) {
			if (!ModelState.IsValid) return BadRequest();

			var serviceModel = _mapper.Map<StudentAddModel>(request);
			var result = await _service.Add(serviceModel);

			return Ok(_mapper.Map<StudentResponse>(result));
		}

		[HttpGet("{id}")]
		public async Task<StudentResponse> Get(int id) {
			var result = await _service.Get(id);
			return _mapper.Map<StudentResponse>(result);
		}

		[HttpPost("list")]
		public async Task<IActionResult> GetList(StudentGetListModel model) {
			if (!ModelState.IsValid) return BadRequest();

			var result = await _service.GetList(model);

			return Ok(result);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> Update(int id, StudentUpdateRequest request) {
			if (!ModelState.IsValid) return BadRequest();

			var serviceModel = _mapper.Map<StudentUpdateModel>(request);
			serviceModel.Id = id;
			var result = await _service.Update(serviceModel);

			return Ok(_mapper.Map<StudentResponse>(result));
		}

		[HttpDelete("{id}")]
		public Task Update(int id) {
			return _service.Delete(id);
		}
	}
}