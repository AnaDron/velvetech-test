namespace Test.Api.Models {
	public class GroupResponse {
		public int Id { get; set; }
		public string Name { get; set; }
	}
}