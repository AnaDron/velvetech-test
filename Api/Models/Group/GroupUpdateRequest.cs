using System.ComponentModel.DataAnnotations;

namespace Test.Api.Models {
	public class GroupUpdateRequest {
		[MaxLength(25)]
		public string Name { get; set; }
	}
}