using System.ComponentModel.DataAnnotations;

namespace Test.Api.Models {
	public class GroupAddRequest {
		[Required]
		[MaxLength(25)]
		public string Name { get; set; }
	}
}