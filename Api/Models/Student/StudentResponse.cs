using Test.Domain.Enums;

namespace Test.Api.Models {
	public class StudentResponse {
		public int Id { get; set; }
		public Gender Gender { get; set; }
		public string LastName { get; set; }
		public string FirstName { get; set; }
		public string Patronymic { get; set; }
		public string Uid { get; set; }
	}
}