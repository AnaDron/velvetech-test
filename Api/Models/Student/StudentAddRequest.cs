using System.ComponentModel.DataAnnotations;
using Test.Domain.Enums;

namespace Test.Api.Models {
	public class StudentAddRequest {
		[Required]
		public Gender? Gender { get; set; }

		[Required]
		[MaxLength(40)]
		public string LastName { get; set; }

		[Required]
		[MaxLength(40)]
		public string FirstName { get; set; }

		[MaxLength(60)]
		public string Patronymic { get; set; }

		[MinLength(6)]
		[MaxLength(16)]
		public string Uid { get; set; }
	}
}