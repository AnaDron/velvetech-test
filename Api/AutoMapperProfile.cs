﻿using AutoMapper;
using Test.Api.Models;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.Api {
	class AutoMapperProfile : Profile {
		public AutoMapperProfile() {
			#region Student

			CreateMap<StudentAddRequest, StudentAddModel>();
			CreateMap<StudentUpdateRequest, StudentUpdateModel>();
			CreateMap<Student, StudentResponse>();

			#endregion
			
			#region Group

			CreateMap<GroupAddRequest, GroupAddModel>();
			CreateMap<GroupUpdateRequest, GroupUpdateModel>();
			CreateMap<Group, GroupResponse>();

			#endregion
		}
	}
}