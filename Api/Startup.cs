using System;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Test.BLL;
using Test.DAL;
using Test.Domain.Configuration;

namespace Test.Api {
	public class Startup {
		readonly IConfiguration _configuration;

		public Startup(IConfiguration configuration) {
			_configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services) {
			services.AddControllers();

			services.AddDAL(_configuration);
			services.AddBLL();

			services.AddAutoMapper(typeof(AutoMapperProfile));

			var jwtSection = _configuration.GetSection("JwtBearerTokenSettings");
			services.Configure<JwtBearerTokenSettings>(jwtSection);

			var jwtBearerTokenSettings = jwtSection.Get<JwtBearerTokenSettings>();

			services.AddAuthentication(x => {
					x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
					x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
				})
				.AddJwtBearer(x => {
					x.RequireHttpsMetadata = false;
					x.SaveToken = true;
					x.TokenValidationParameters = new TokenValidationParameters {
						ValidIssuer = jwtBearerTokenSettings.Issuer,
						ValidAudience = jwtBearerTokenSettings.Audience,
						IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtBearerTokenSettings.SecretKey)),
						ClockSkew = TimeSpan.Zero
					};
				});

			services.AddSwaggerGen(c => {
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "Test API", Version = "v1" });
				c.EnableAnnotations();
			});
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseSwagger();

			app.UseSwaggerUI(c => {
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");
			});

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints => {
				endpoints.MapControllers();
			});
		}
	}
}