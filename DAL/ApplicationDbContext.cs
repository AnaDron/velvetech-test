﻿using Microsoft.EntityFrameworkCore;
using Test.DAL.Entities;

namespace Test.DAL {
	class ApplicationDbContext : DbContext {
		public ApplicationDbContext() { }

		public ApplicationDbContext(DbContextOptions options) : base(options) { }

		public virtual DbSet<StudentEntity> Students { get; set; }
		public virtual DbSet<GroupEntity> Groups { get; set; }
		public virtual DbSet<GroupStudentRelation> GroupStudents { get; set; }

		protected override void OnModelCreating(ModelBuilder builder) {
			base.OnModelCreating(builder);

			builder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
		}
	}
}