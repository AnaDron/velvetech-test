﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Test.DAL.Entities;
using Test.Domain.Data;
using Test.Domain.Entities;

namespace Test.DAL.Data {
	sealed class GroupStudentRepository : RepositoryBase<GroupStudent, GroupStudentRelation>, IGroupStudentRepository {
		public GroupStudentRepository(
			ApplicationDbContext context,
			IMapper mapper,
			ILogger<GroupStudentRepository> logger
		) : base(context, mapper, logger) {
		}

		public new async Task Delete(GroupStudent entity) {
			try {
				await base.Delete(entity);
			} catch (DbUpdateConcurrencyException) {
			}
		}
	}
}