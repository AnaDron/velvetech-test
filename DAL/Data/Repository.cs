﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Test.DAL.Entities;
using Test.Domain.Data;

namespace Test.DAL.Data {
	abstract class Repository<TModel, TEntity> : RepositoryBase<TModel, TEntity>, IRepository<TModel> where TEntity : EntityBase, new() {
		protected Repository(
			ApplicationDbContext context,
			IMapper mapper,
			ILogger logger
		) : base(context, mapper, logger) {
		}

		public async Task<TModel> Get(int id) {
			var result = await Set.FindAsync(id);
			return Mapper.Map<TModel>(result);
		}

		public async Task<TModel> Update(TModel entity) {
			var dbEntity = Mapper.Map<TEntity>(entity);
			var result = Set.Update(dbEntity);
			await Context.SaveChangesAsync();
			return Mapper.Map<TModel>(result.Entity);
		}

		public async Task Delete(int id) {
			await Delete(new TEntity { Id = id });
		}
	}
}