﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Test.DAL.Entities;

namespace Test.DAL.Data {
	abstract class RepositoryBase<TModel, TEntity> where TEntity: class {
		protected RepositoryBase(
			ApplicationDbContext context,
			IMapper mapper,
			ILogger logger
		) {
			Context = context;
			Set = Context.Set<TEntity>();
			Mapper = mapper;
			Logger = logger;
		}

		protected ApplicationDbContext Context { get; }
		protected DbSet<TEntity> Set { get; }
		protected IMapper Mapper { get; }
		protected ILogger Logger { get; }

		public async Task<TModel> Add(TModel entity) {
			var dbEntity = Mapper.Map<TEntity>(entity);
			var result = await Set.AddAsync(dbEntity);
			await Context.SaveChangesAsync();
			return Mapper.Map<TModel>(result.Entity);
		}

		protected async Task Delete(TModel entity) {
			var dbEntity = Mapper.Map<TEntity>(entity);
			await Delete(dbEntity);
		}

		protected async Task Delete(TEntity entity) {
			Set.Remove(entity);
			await Context.SaveChangesAsync();
		}
	}
}