﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Test.DAL.Entities;
using Test.Domain.Data;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.DAL.Data {
	sealed class StudentRepository : Repository<Student, StudentEntity>, IStudentRepository {
		public StudentRepository(
			ApplicationDbContext context,
			IMapper mapper,
			ILogger<StudentRepository> logger
		) : base(context, mapper, logger) {
		}

		public async Task<IEnumerable<StudentViewModel>> GetList(StudentGetListModel model) {
			IQueryable<StudentEntity> query = Set.AsNoTracking()
				.Include(x => x.GroupStudents)
				.ThenInclude(x => x.Group);

			if (model.Filter.Gender != null) {
				query = query.Where(x => x.Gender == model.Filter.Gender);
			}

			if (model.Filter.Name != null) {
				query = query.Where(x => x.LastName.Contains(model.Filter.Name)
				                         || x.FirstName.Contains(model.Filter.Name)
				                         || x.Patronymic.Contains(model.Filter.Name));
			}

			if (model.Filter.Uid != null) {
				query = query.Where(x => x.Uid.Contains(model.Filter.Uid));
			}

			if (model.Filter.GroupName != null) {
				query = query.Where(x => x.GroupStudents.Any(y => y.Group.Name.Contains(model.Filter.GroupName)));
			}

			var result = await query
				.Skip(model.Pagination.Page * model.Pagination.PerPage)
				.Take(model.Pagination.PerPage)
				.ToListAsync();

			return Mapper.Map<IEnumerable<StudentViewModel>>(result);
		}
	}
}