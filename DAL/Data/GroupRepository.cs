﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Test.DAL.Entities;
using Test.Domain.Data;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.DAL.Data {
	sealed class GroupRepository : Repository<Group, GroupEntity>, IGroupRepository {
		public GroupRepository(
			ApplicationDbContext context,
			IMapper mapper,
			ILogger<GroupRepository> logger
		) : base(context, mapper, logger) {
		}

		public async Task<IEnumerable<GroupViewModel>> GetList(GroupGetListModel model) {
			var query = Set.AsNoTracking()
				.ProjectTo<GroupViewModel>(Mapper.ConfigurationProvider);

			if (model.Filter.Name != null) {
				query = query.Where(x => x.Name.Contains(model.Filter.Name));
			}

			var result = await query
				.Skip(model.Pagination.Page * model.Pagination.PerPage)
				.Take(model.Pagination.PerPage)
				.ToListAsync();

			return result;
		}
	}
}