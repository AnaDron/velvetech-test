﻿using System.Linq;
using AutoMapper;
using Test.DAL.Entities;
using Test.Domain.Entities;
using Test.Domain.Models;

namespace Test.DAL {
	class AutoMapperProfile : Profile {
		public AutoMapperProfile() {
			#region Student

			CreateMap<Student, StudentEntity>().ReverseMap();
			CreateMap<StudentEntity, StudentViewModel>()
				.ForMember(x => x.Name, map => map.MapFrom(x => $"{x.LastName} {x.FirstName} {x.Patronymic}"))
				.ForMember(x => x.Groups, map => map.MapFrom(x => string.Join(", ", x.GroupStudents.Select(y => y.Group.Name))));

			#endregion
			
			#region Group

			CreateMap<Group, GroupEntity>().ReverseMap();
			CreateMap<GroupEntity, GroupViewModel>()
				.ForMember(x => x.StudentsCount, map => map.MapFrom(x => x.GroupStudents.Count));

			#endregion

			#region GroupStudent

			CreateMap<GroupStudent, GroupStudentRelation>().ReverseMap();

			#endregion
		}
	}
}