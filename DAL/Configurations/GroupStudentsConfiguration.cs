using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Test.DAL.Entities;

namespace Test.DAL.Configurations {
	sealed class GroupStudentsConfiguration : IEntityTypeConfiguration<GroupStudentRelation> {
		public void Configure(EntityTypeBuilder<GroupStudentRelation> builder) {
			builder.HasKey(ci => new { ci.GroupId, ci.StudentId });
		}
	}
}