using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Test.DAL {
	public static class ServiceProviderExtension {
		public static async Task InitializeDAL(this IServiceProvider self) {
			using var scope = self.CreateScope();

			var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
			await context.Database.MigrateAsync().ConfigureAwait(false);
		}
	}
}