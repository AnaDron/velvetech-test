namespace Test.DAL.Entities {
	class GroupStudentRelation {
		public int GroupId { get; set; }
		public int StudentId { get; set; }

		public GroupEntity Group { get; set; }
		public StudentEntity Student { get; set; }
	}
}