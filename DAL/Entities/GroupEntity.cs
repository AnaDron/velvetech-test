using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Test.DAL.Entities {
	class GroupEntity : EntityBase {
		[Required]
		[MaxLength(25)]
		public string Name { get; set; }

		public ICollection<GroupStudentRelation> GroupStudents { get; set; } = new List<GroupStudentRelation>();
	}
}