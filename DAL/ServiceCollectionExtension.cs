using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Test.DAL.Data;
using Test.Domain.Data;

namespace Test.DAL {
	public static class ServiceCollectionExtension {
		public static IServiceCollection AddDAL(this IServiceCollection self, IConfiguration configuration) {
			self.AddDbContext<ApplicationDbContext>(options => {
				options.UseSqlServer(
					configuration.GetConnectionString("SqlConnection"),
					b => b.MigrationsAssembly("DAL")
				);
			});

			self.AddScoped<IStudentRepository, StudentRepository>();
			self.AddScoped<IGroupRepository, GroupRepository>();
			self.AddScoped<IGroupStudentRepository, GroupStudentRepository>();

			self.AddAutoMapper(typeof(AutoMapperProfile));

			return self;
		}
	}
}